﻿using ma_tricks_backend.Models;
using System.Threading.Tasks;

namespace ma_tricks_backend.Services
{
    public interface IMatrixService
    {
        Task<MatrixOutputModel> Reduce(MatrixInputModel matrix);
    }
}
