﻿using System.Collections.Generic;
using System.Linq;

namespace ma_tricks_backend.Models
{
    public class Matrix
    {
        public IEnumerable<Row> Rows { get; set; }

        public int RowCount() => Rows.Count();
        public int ColumnCount() => Rows.First().Entries.Count();

        public Matrix()
        {

        }
    }
}
