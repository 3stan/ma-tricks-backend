﻿using System.Collections.Generic;

namespace ma_tricks_backend.Models
{
    public class Row
    {
        public IEnumerable<double> Entries { get; set; }
    }
}
