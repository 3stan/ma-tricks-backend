﻿using System.Collections.Generic;

namespace ma_tricks_backend.Models
{
    public class MatrixInputModel
    {
        public IEnumerable<Row> Rows { get; set; }
    }

    public class MatrixOutputModel : MatrixInputModel { }
}
