﻿using ma_tricks_backend.Models;
using ma_tricks_backend.Services;
using Microsoft.AspNetCore.Mvc;
using System.Collections;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace ma_tricks_backend.Controllers
{
    [ApiController]
    public class MatrixController : ControllerBase
    {
        private readonly IMatrixService _matrixService;
        public MatrixController(IMatrixService matrixService)
        {
            this._matrixService = matrixService;
        }

        [Route("api/matrix/reduce")]
        [HttpPost]
        public async Task<ActionResult<MatrixOutputModel>> ReduceAsync([FromBody] MatrixInputModel matrix)
        {
            return await _matrixService.Reduce(matrix);
        }

        [Route("api/matrix/")]
        [HttpGet]
        public ActionResult<IEnumerable<string>> Get()
        {
            return new string[] { "you", "made it" };
        }
    }
}